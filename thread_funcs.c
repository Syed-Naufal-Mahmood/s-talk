#define _POSIX_C_SOURCE 200112L
#include <unistd.h> 
#include <string.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include "thread_funcs.h"
#include "list.h"
#include <stdlib.h>
#include <pthread.h>

#define BUFF_LEN 100

pthread_mutex_t mutex_read_send = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_receive_print = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_read_send = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_receive_print = PTHREAD_COND_INITIALIZER;
int exitVar = 1; 


void* read_message(void* temp) {
	LIST* listHandle = (LIST*)temp;
	int numbytes;
	do {
		char* buf = malloc(sizeof(char) * BUFF_LEN);
		buf[0] = '\0'; 
		numbytes = read(STDIN_FILENO, buf, sizeof(char) * BUFF_LEN);

		if (!strcmp(buf, "!\n")) {
			pthread_cancel(tReceiver);
			pthread_cancel(tPrinter);
			exitVar = 0; 
		}

		pthread_mutex_lock(&mutex_read_send);

		ListPrepend(listHandle, buf);

		pthread_cond_signal(&cond_read_send);
		pthread_mutex_unlock(&mutex_read_send); 
	} while(exitVar);
}

void* send_message(void* temp) {
	struct sender_data* data = (struct sender_data*) temp; 
	char* message; 
	int numbytes = 0;
	do {
		pthread_mutex_lock(&mutex_read_send);
		pthread_cond_wait(&cond_read_send, &mutex_read_send);

		message = (char*)(data->listHandle->tail->data);
		message[strlen(message)] = '\0';
		if ((numbytes = sendto(data->socketfd, (char*)message, strlen(message), 0, data->p->ai_addr, data->p->ai_addrlen)) == -1) {
			perror("talker: sendto");
		}

		ListLast(data->listHandle);
		free(data->listHandle->tail->data);
		ListTrim(data->listHandle);

		pthread_mutex_unlock(&mutex_read_send); 
	} while (exitVar); 
	char* stop = "!\n";
	sendto(data->socketfd, stop, strlen(stop), 0, data->p->ai_addr, data->p->ai_addrlen);
}

void* receive_message(void* temp) {
	struct receiver_data* data = (struct receiver_data*) temp; 
	int numbytes = 0;
	socklen_t addr_len = sizeof data->theirAddr;
	do {
		char* buf = malloc(sizeof(char) * BUFF_LEN);
		buf[0] = '\0';

		if ((numbytes = recvfrom(data->socketfd, buf, BUFF_LEN - 1, 0,
			(struct sockaddr*) data->theirAddr, &addr_len)) == -1) {
			perror("recvfrom");
		}

		if (!strcmp(buf, "!\n")) {
			pthread_cancel(tSender);
			pthread_cancel(tReader);
			pthread_cancel(tPrinter);
			pthread_cancel(tReceiver);
			exitVar = 0;
		}

		pthread_mutex_lock(&mutex_receive_print);

		ListPrepend(data->listHandle, buf);

		pthread_cond_signal(&cond_receive_print);
		pthread_mutex_unlock(&mutex_receive_print); 
	} while (exitVar); 
}


void* print_message(void* temp) {
	LIST* listHandle = (LIST*)temp; 
	char* message;
	char* rem = "remote: ";
	int rv;
	do {
		pthread_mutex_lock(&mutex_receive_print); 
		pthread_cond_wait(&cond_receive_print, &mutex_receive_print);

		message = (char*)(listHandle->tail->data);
		char* fullMsg = malloc(strlen(rem) + strlen((char*)(listHandle->tail->data))+1);
		fullMsg[0] = '\0';
		strcat(fullMsg, rem);
		strcat(fullMsg, (char*)(listHandle->tail->data));
		rv = write(STDOUT_FILENO, fullMsg, strlen(fullMsg));

		free(fullMsg); 
		ListLast(listHandle);
		free(listHandle->tail->data);
		ListTrim(listHandle);

		pthread_mutex_unlock(&mutex_receive_print); 

		rv = write(STDOUT_FILENO, fullMsg, strlen(fullMsg));
	} while (exitVar); 
}


void destroy_mutexes() {
	pthread_mutex_destroy(&mutex_read_send); 
	pthread_mutex_destroy(&mutex_receive_print);
	pthread_cond_destroy(&cond_read_send);
	pthread_cond_destroy(&cond_receive_print);
}

void freeMem(LIST* listHandle) {
	if (listHandle->head != NULL) {
		NODE* traverser = listHandle->head;
		while (traverser->nextNode != NULL) {
			free(traverser->data);
			traverser = traverser->nextNode;
		}
	}
}

void cleanup(LIST* listHandle1, LIST* listHandle2) {
	freeMem(listHandle1); 
	freeMem(listHandle2);
	ListFree(listHandle1, itemFree); 
	ListFree(listHandle2, itemFree); 
}

