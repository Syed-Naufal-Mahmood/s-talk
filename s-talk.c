/*
UDP code based off of Beej's tutorial: 
http://beej.us/guide/bgnet/pdf/bgnet_usl_c_1.pdf
https://beej.us/guide/bgnet/examples/listener.c
https://beej.us/guide/bgnet/examples/talker.c
*/
#define _POSIX_C_SOURCE 200112L
#include "list.h" 
#include <stdio.h> 
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include "thread_funcs.h"


int main(int argc, char** argv) {
	/*
	1: my port num
	2: remote machine name
	3: remote port num
	*/
	int myPortNum = atoi(argv[1]);
	int remotePortNum = atoi(argv[3]);
	char* remMachineName = malloc(strlen(argv[2]));
	strcpy(remMachineName, argv[2]);

	/*vars for sending */
	int remSocketfd;
	struct addrinfo remHints, * remServinfo, * remP;
	int rv;
	int numbytes;
	LIST* sendListHandle = ListCreate();

	/*vars up for receiving*/
	int mySocketfd;
	struct addrinfo myHints, * myServinfo, * myP;
	struct sockaddr_storage their_addr;
	socklen_t addr_len;
	char s[INET6_ADDRSTRLEN];
	LIST* recvListHandle = ListCreate();

	char* message = "Start Chatting!\n";
	write(STDOUT_FILENO, message, strlen(message));

	/*set up UDP structures for sending*/
	memset(&remHints, 0, sizeof remHints); 
	remHints.ai_family = AF_INET; 
	remHints.ai_socktype = SOCK_DGRAM; 

	if ((rv = getaddrinfo(remMachineName, argv[3], &remHints, &remServinfo)) != 0) {
		printf("getaddrinfo returnval %d\n", rv);
		return 1; 
	}
	
	/*AF_INET: IPv4 protocol - family
	SOCK_DGRAM: specify datagram support - socktype
	0:chooses correct protocol for the given type (datagram in this case)
	returns a socket descriptor */
	for (remP = remServinfo; remP != NULL; remP = remP->ai_next) {
		if ((remSocketfd = socket(remP->ai_family, remP->ai_socktype, remP->ai_protocol)) == -1) {
			perror("talker: socket"); 
			continue; 
		}
		break; 
	}
	
	if (remP == NULL) {
		fprintf(stderr, "talker: failed to create/bind socket\n");
		return 2; 
	}

	/*set up UDP structures for sending*/
	memset(&myHints, 0, sizeof myHints);
	myHints.ai_family = AF_INET; // set to AF_INET to force IPv4
	myHints.ai_socktype = SOCK_DGRAM;
	myHints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, argv[1], &myHints, &myServinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for (myP = myServinfo; myP != NULL; myP = myP->ai_next) {
		if ((mySocketfd = socket(myP->ai_family, myP->ai_socktype,
			myP->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

		if (bind(mySocketfd, myP->ai_addr, myP->ai_addrlen) == -1) {
			close(mySocketfd);
			perror("listener: bind");
			continue;
		}

		break;
	}

	if (myP == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	/*set up data structures to pass to threads */
	struct sender_data sender;
	sender.listHandle = sendListHandle;
	sender.p = remP;
	sender.socketfd = remSocketfd;

	struct receiver_data receiver;
	receiver.listHandle = recvListHandle;
	receiver.theirAddr = &their_addr;
	receiver.socketfd = mySocketfd;

	/*setup threading*/ 
	pthread_create(&tReader, NULL, read_message, (void*)sendListHandle);
	pthread_create(&tReceiver, NULL, receive_message, (void*)&receiver);
	pthread_create(&tSender, NULL, send_message, (void*)&sender);
	pthread_create(&tPrinter, NULL, print_message, (void*)recvListHandle);

	pthread_join(tReader, NULL);
	pthread_join(tReceiver, NULL); 
	pthread_join(tSender, NULL);
	pthread_join(tPrinter, NULL);
	destroy_mutexes(); 

	/*clean up*/
	freeaddrinfo(remServinfo);
	freeaddrinfo(myServinfo);
	close(remSocketfd);
	close(mySocketfd);
	free(remMachineName);
	cleanup(recvListHandle, sendListHandle);
	
	message = "Session Ended\n";
	write(STDOUT_FILENO, message, strlen(message));
	

	return 0; 
}