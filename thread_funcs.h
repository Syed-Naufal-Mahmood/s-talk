#define _POSIX_C_SOURCE 200112L
#ifndef THREAD_FUNCS
#define THREAD_FUNCS 
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "list.h"
pthread_t tReader, tSender, tReceiver, tPrinter;

struct sender_data {
	LIST* listHandle;
	int socketfd; 
	struct addrinfo* p; 

};

struct receiver_data {
	LIST* listHandle; 
	int socketfd; 
	struct sockaddr_storage* theirAddr; 
};

void* read_message(void* temp); 

void* send_message(void* temp); 

void* receive_message(void* temp);

void* print_message(void* temp);

void destroy_mutexes(); 

void cleanup(LIST* listHandle1, LIST* listHandle2);
#endif // !THREAD_FUNCS

