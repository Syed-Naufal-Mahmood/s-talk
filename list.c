#include <stdio.h>
#include "list.h"
#define NODE_ARRAY_SIZE 10000
#define HEAD_ARRAY_SIZE 2

static NODE nodeArray[NODE_ARRAY_SIZE];
static LIST headArray[HEAD_ARRAY_SIZE];
static NODE* freeNodeList = nodeArray;
static LIST* freeListHeadsList = headArray;
int arrayInitialized = 0; 

NODE* getNode(LIST* l1) {
	NODE* returnNode = freeNodeList; 
	freeNodeList = freeNodeList->nextNode; 
	returnNode->nextNode = NULL; 
	returnNode->prevNode = NULL; 
	return returnNode; 
}

void addToFreeNodeList(NODE* node) {
	node->nextNode = NULL;
	node->prevNode = NULL;
	if (freeNodeList == NULL) {
		freeNodeList = node; 
	}
	else {
		freeNodeList->prevNode = node;
		node->nextNode = freeNodeList;
		freeNodeList = freeNodeList->prevNode;
	}
}

void addToFreeListHeadsList(LIST* list) {
	list->current = NULL;
	list->head = NULL;
	list->tail = NULL;
	list->listSize = 0;
	list->place = uninitialized;
	if (freeListHeadsList == NULL) {
		freeListHeadsList = list;
	}
	else {
		list->nextList = freeListHeadsList;
		freeListHeadsList->prevList = list;
		freeListHeadsList = freeListHeadsList->prevList;
	}
}

int initList(LIST* list, void* item) {
	list->head = freeNodeList;
	freeNodeList = freeNodeList->nextNode; 
	list->head->nextNode = NULL; 
	list->head->prevNode = NULL;
	list->current = list->head; 
	list->tail = list->head;
	list->place = inList;
	list->current->data = item;
	list->listSize++;

	return 0; 
}

LIST* ListCreate() { /*needs to return null if fails */
	if (arrayInitialized == 0) {
		arrayInitialized = 1; 
		int i = 0;
		for (i = 0; i < HEAD_ARRAY_SIZE; i++) {
			if (i == (HEAD_ARRAY_SIZE - 1)) {
				headArray[i].prevList = &headArray[i - 1];
				headArray[i].nextList = NULL;
			}
			else if (i == 0) {
				headArray[i].prevList = NULL;
				headArray[i].nextList = &headArray[i + 1];
			}
			else {
				headArray[i].prevList = &headArray[i - 1];
				headArray[i].nextList = &headArray[i + 1];
			}
		}
		for (i = 0; i < (NODE_ARRAY_SIZE); i++) {
			if (i == (NODE_ARRAY_SIZE - 1)) {
				nodeArray[i].prevNode = &nodeArray[i - 1];
				nodeArray[i].nextNode = NULL;
			}
			else if (i == 0) {
				nodeArray[i].prevNode = NULL;
				nodeArray[i].nextNode = &nodeArray[i + 1];
			}
			else {
				nodeArray[i].prevNode = &nodeArray[i - 1];
				nodeArray[i].nextNode = &nodeArray[i + 1];
			}
		}
	}
	if (freeListHeadsList == NULL) {
		return NULL;
	}
	else {
		LIST* returnVal = freeListHeadsList; /* is a free node in the headArray */
		freeListHeadsList = freeListHeadsList->nextList;
		/* remove the node from the linked list in the array */
		returnVal->prevList = NULL;
		returnVal->nextList = NULL;

		returnVal->place = inList;

		return returnVal;
	}
}

int ListCount(LIST* list) {
	return list->listSize;
}

void* ListFirst(LIST* list) {
	if (list->head == NULL) {
		list->current = list->head;
		return NULL; 
	} 
	list->current = list->head;
	return list->head;
}

void* ListLast(LIST* list) {
	if (list->tail == NULL) {
		list->current = list->tail;
		return NULL; 
	}
	else {
		list->current = list->tail; 
		return list->tail;
	}
}

void* ListNext(LIST* list) { /* return NULL if goes beyond list */
	if (list->current->nextNode == NULL) {
		list->place = afterList; 
		return NULL;
	}
	else if (list->place == beforeList) {
		return list->current; 
	}
	else {
		list->current = list->current->nextNode; 
		return list->current;
	}
}

void* ListPrev(LIST* list) {
	if (list->current->prevNode == NULL) {
		list->place = beforeList; 
		return NULL; 
	}
	else if (list->place == afterList) {
		return list->current; 
	}
	else {
		list->current = list->current->prevNode; 
		return list->current;
	}
}

void* ListCurr(LIST* list) {
	if (list->place == afterList || list->place == beforeList) {
		return NULL;
	} 
	else {
		return list->current;
	}
}

int ListAdd(LIST* list, void* item) {
	int returnVal = -1; 
	if (freeNodeList == NULL) {
		return returnVal; 
	}
	else if (list->listSize == 0) {
		returnVal = initList(list, item);
		return returnVal; 
	} 
	else if (list->place == beforeList) {
		returnVal = ListPrepend(list, item); 
		return returnVal; 
	}
	else if (list->place == afterList || list->current->nextNode == NULL) {
		returnVal = ListAppend(list, item); 
		return returnVal; 
	}
	else {
		NODE* temp = list->current->nextNode;
		list->current->nextNode = getNode(list);
		list->current->nextNode->prevNode = list->current;
		list->current->nextNode->nextNode = temp;
		list->current->nextNode->nextNode->prevNode = list->current->nextNode;

		list->current = list->current->nextNode;
		list->current->data = item;
		list->listSize++;

		return 0; 
	}
}

int ListInsert(LIST* list, void* item) {
	int returnVal = -1;
	if (freeNodeList == NULL) {
		return returnVal;
	}
	else if (list->listSize == 0) {
		returnVal = initList(list, item);
		return returnVal;
	}
	else if (list->place == beforeList || list->current->prevNode == NULL) {
		returnVal = ListPrepend(list, item);
		return returnVal;
	}
	else if (list->place == afterList) {
		returnVal = ListAppend(list, item);
		return returnVal;
	}
	else {
		NODE* temp = list->current->prevNode; 
		list->current->prevNode = getNode(list); 
		list->current->prevNode->nextNode = list->current; 
		list->current->prevNode->prevNode = temp; 
		list->current->prevNode->prevNode->nextNode = list->current->prevNode; 

		list->current = list->current->prevNode; 
		list->current->data = item; 
		list->listSize++;

		return 0; 
	}
} 

int ListAppend(LIST* list, void* item) {
	if (freeNodeList == NULL) {
		return -1;
	}
	else if (list->listSize == 0) {
		initList(list, item); 
		return 0; 
	} 
	else {
		list->tail->nextNode = getNode(list);
		list->tail->nextNode->prevNode = list->tail;
		list->tail = list->tail->nextNode;

		list->current = list->tail;
		list->current->data = item;
		list->listSize++;

		return 0;
	}
}

int ListPrepend(LIST* list, void* item) {
	if (freeNodeList == NULL) {
		return -1;
	}
	if (list->listSize == 0) {
		initList(list, item);
		return 0;
	}
	else {
		list->head->prevNode = getNode(list); 
		list->head->prevNode->nextNode = list->head; 
		list->head = list->head->prevNode; 

		list->current = list->head; 
		list->current->data = item; 
		list->listSize++; 

		return 0;
	}
}

void* ListRemove(LIST* list) {
	if (list->place == afterList || list->place == beforeList) {
		return NULL; 
	}
	else if (list->current == list->tail) {
		void* returnVal = ListTrim(list); 
		list->place = afterList;
		return returnVal; 
	} 
	else {
		NODE* temp = list->current;
		list->current = list->current->nextNode;
		temp->prevNode->nextNode = list->current;
		list->current->prevNode = temp->prevNode;

		addToFreeNodeList(temp); 
		list->listSize--; 
		return temp->data;
	}
}

void ListConcat(LIST* list1, LIST* list2) { /*user isnt malicious, so they still would have a reference to list2 */
	/*add list2 to the end of list1*/ 
	list1->tail->nextNode = list2->head; 
	list2->head->prevNode = list1->tail; 
	list1->tail = list2->tail; 

	list1->listSize = list1->listSize + list2->listSize; 
	addToFreeListHeadsList(list2); 
}

void ListFree(LIST* list, void (*itemFree)(NODE*)) {
	if (list->tail != NULL) {
		NODE* traverser = list->tail;
		NODE* temp = NULL;
		while (traverser->prevNode != NULL) {
			itemFree(traverser);
			temp = traverser;
			traverser = traverser->prevNode;
			addToFreeNodeList(temp);
		}
		addToFreeListHeadsList(list);
	}
}	

void* ListTrim(LIST* list) {
	if (list->listSize == 1) {
		NODE* temp = list->tail;
		list->tail = NULL; 
		list->current = NULL; 
		list->head = NULL; 

		addToFreeNodeList(temp); 
		list->listSize--;
		return temp->data;
	}
	else {
		NODE* temp = list->tail;
		list->tail = list->tail->prevNode;
		list->current = list->tail;
		list->tail->nextNode = NULL;

		addToFreeNodeList(temp);
		list->listSize--;
		return temp->data;
	}
}

void* ListSearch(LIST* list, int (*comparator)(void*, void*), void* comparisonArg) {
	NODE* traverser = list->head;
	int comparisonEval = 0; 
	while (traverser != NULL) {
		comparisonEval = comparator(traverser->data, comparisonArg); 
		if (comparisonEval == 1) {
			list->current = traverser; 
			return list->current; 
		}
		traverser = traverser->nextNode;
	}
	list->current = list->tail; 
	list->place = afterList; 
	return NULL; 
}

void itemFree(NODE* node) { /*assigns value of 0 to any standard C type */
	node->data = NULL; 
}

int comparisonFunction(void* arg1, void* arg2) {
	if (arg1 == arg2) {
		return 1; 
	}
	else {
		return 0; 
	}
}